package toDo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import toDo.model.ProgressOfTask;
import toDo.model.Task;
import toDo.model.User;
import toDo.repository.TaskRepository;
import toDo.repository.UserRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@EnableAutoConfiguration
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;


    @Autowired
    private UserRepository userRepository;

    @GetMapping("/registration")
    public String registration(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "registration";
    }


    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/registry")
    public String showForm(User user) {
        return "registration";
    }

    @PostMapping("/registry")
    public String addValidUser(@ModelAttribute("user") @Valid User user,
                               BindingResult bindingResult) {
        if (!user.getPassword().equals(user.getPasswordConfirm())) {
            bindingResult.rejectValue("password", "Diff.userForm.passwordConfirm", "Hasla nie sa takie same");
            return "registration";
        }
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        User findExisting = userRepository.findByUsername(user.getUsername());

        if (findExisting != null) {
            bindingResult.rejectValue("username", "Duplicate.userForm.username", "Nazwa zostala juz zajeta");
            return "registration";
        }
        userRepository.save(user);
        return "login";

    }

    @PostMapping("/addNewTask")
    public String addNewTask(@ModelAttribute("task") @Valid Task task,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/allTasks";
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        String taskName = task.getTaskName().trim();
        task.setTaskName(taskName);
        task.setDone(ProgressOfTask.ToDo);
        User user = userRepository.findByUsername(username);
        task.setUser(user);
        user.getTaskList().add(task);
        taskRepository.save(task);
        return "redirect:/allTasks";
    }

    @GetMapping("/setDone")
    public String setTaskAsDone(@RequestParam(value = "id") Long id) {
        Task task = taskRepository.findById(id);
        task.setDone(ProgressOfTask.Done);
        taskRepository.save(task);
        return "redirect:/allTasks";
    }

    @GetMapping("/setInProgress")
    public String setTaskAsInProgress(@RequestParam(value = "id") Long id) {
        Task task = taskRepository.findById(id);
        task.setDone(ProgressOfTask.InProgress);
        taskRepository.save(task);
        return "redirect:/allTasks";
    }

    @GetMapping("/setToDo")
    public String setTaskAsToDo(@RequestParam(value = "id") Long id) {
        Task task = taskRepository.findById(id);
        task.setDone(ProgressOfTask.ToDo);
        taskRepository.save(task);
        return "redirect:/allTasks";
    }

    @GetMapping("/deleteTask")
    public String deleteTask(@RequestParam(value = "id") Long id) {
        taskRepository.delete(id);
        return "redirect:/allTasks";
    }

    @GetMapping("/allTasks")
    public String getAllTasks(Model model) {
        Task task = new Task();
        model.addAttribute("task", task);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        List<Task> allToDoTasks = new ArrayList<>();
        List<Task> allInProgressTasks = new ArrayList<>();
        List<Task> allDoneTasks = new ArrayList<>();
        model.addAttribute("allTasks", taskRepository.findAll());
        for (Task t : taskRepository.findAll()) {
            if (t.getUser().equals(userRepository.findByUsername(username))) {
                if (t.getDone().equals(ProgressOfTask.ToDo)) {
                    allToDoTasks.add(t);
                } else if (t.getDone().equals(ProgressOfTask.InProgress)) {
                    allInProgressTasks.add(t);
                } else if (t.getDone().equals(ProgressOfTask.Done)) {
                    allDoneTasks.add(t);
                } else {
                    System.out.println("Wrong progress Of Task entity");
                }
            }
        }
        model.addAttribute("allToDoTasks", allToDoTasks);
        model.addAttribute("allInProgressTasks", allInProgressTasks);
        model.addAttribute("allDoneTasks", allDoneTasks);
        return "mainView";
    }
}
