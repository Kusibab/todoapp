package toDo.validation;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PasswordValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordLevelConstraint {
    String message() default "Password must consist number and capital letter!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
