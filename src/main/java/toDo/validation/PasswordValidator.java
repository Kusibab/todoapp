package toDo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<PasswordLevelConstraint, String> {

    @Override
    public void initialize(PasswordLevelConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value != null && value.matches(".*\\d+.*") && value.matches("(?s).*[A-Z].*");
    }
}