package toDo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import toDo.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    Task findById(Long id);
}
