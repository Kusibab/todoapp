package toDo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import toDo.model.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findById(Long id);

    User findByUsername(String username);

}