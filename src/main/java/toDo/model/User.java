package toDo.model;

import toDo.validation.PasswordLevelConstraint;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_ID")
    private Long id;

    @Column(name = "USERNAME", unique = true)
    @NotNull
    @Size(min = 5, max = 35, message = "Username must consist form 3 to 100 characters")
    private String username;

    @Column(name = "PASSWORD")
    @Size(min = 5, max = 35, message = "Password must consist form 3 to 100 characters")
    @NotNull
    @PasswordLevelConstraint
    private String password;

    @Size(min = 5, max = 35, message = "Password must consist form 3 to 100 characters")
    private String passwordConfirm;

    @Column(name = "ROLE")
    private RoleEnum role;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private List<Task> taskList;

    public User(String username, String password, RoleEnum role, List<Task> taskList) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.taskList = taskList;
    }

    public User() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }


    @Override
    public String toString() {
        return "User [username = " + username + " role = " + role + "]";
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }


    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
