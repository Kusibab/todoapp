package toDo.model;

public enum ProgressOfTask {
    ToDo,
    InProgress,
    Done
}
