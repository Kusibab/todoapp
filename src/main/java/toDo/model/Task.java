package toDo.model;

import javax.persistence.*;
import javax.validation.constraints.Size;


@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @Column(name = "TASK_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TASK_NAME")
    @Size(min = 3, max = 100, message = "Task must consist form 3 to 100 characters")
    private String taskName;

    @Column(name = "DONE")
    private ProgressOfTask done;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public ProgressOfTask getDone() {
        return done;
    }

    public void setDone(ProgressOfTask done) {
        this.done = done;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
